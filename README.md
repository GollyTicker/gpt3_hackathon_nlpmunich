# GPT3-Hackathon

On `05.03.2021` a *GPT-3 Hackathon* is hosted by the [NLP Meetup Group @Munich](https://www.meetup.com/Natural-Language-Processing-Understanding-NLP-NLU/events/276054436/).

After just one and half day, 10 teams pitched their amazing work. According to the pitch oder, they are:



## **Team:** Preste

**Members:** Thibaut Soubrié, Oleksandr Lysenko, Jérémie Lengrais

**Project:** Generate a story from a fictional image

**Summary:** We tested out the potentiality of GPT-3 for text content generation in a 2-step integrated model: that would 1) tag fictional images (image-to-text), then 2) use these tags as an input for GPT-3 in order to generate a story from the image. During the hackathon we tested this on paintings, and had in mind other types of pictures like kid illustrations or comics.

**Pitch Screenshot:** ![image](images/preste.png)

**Code:** coming ...

**Slides:** [here](./slides/preste.pdf)




## Team: SurpriseMe Bot

**Members** Sebastian Loewe, Kun Lu

**Project:** GPT Can Code Creatively

**Summary:** We gave GPT-3 some P5.js (Processing) code examples and it now returns fully functional code snippets back when describing them metaphorically to the model.

**Pitch Screenshot:** ![image](images/supriseme.png)

**Code:** See [this blog on medium](https://sebastian-loewe.medium.com/we-generated-p5-code-based-on-text-inputs-with-openais-gpt-3-and-this-could-show-the-future-of-db2ae41efc8d).



## **Team:** Secret Sauce AI

**Members** Dan Borufka, Justina, Ashit Debdas, Bakhtiar Meraj,Bartmoss St. Clair

**Project:** GPT-3 Language Learning App

**Summary:** Leverage GPT-3 to learn a foreign language

*Did not participate in the pitch*


## Team: MusicMiner

**Members:** Vincent Giardina, Jordi Spranger, Tamara Shnyrina

**Project:** Generate novel song lyrics

**Summary:** Lyrics writing blockade - be GONE! Write lyrics in seconds, get inspired, move the world with your ideas. All you need is a creative spark, be it a word, a name, a sentence or a place. You can choose the style of your favourite artist and your desired song length and you’re DONE.

**Pitch Screenshot:** ![image](images/lyrics.png)

**Code:** See [lyrics.py](src/lyrics.py)

**Slides:** coming ...




## Team: Languagish

**Members:** Alexander Bettenhausen, David Ng, Lukas J. Heidegger, Max Haarich, Jérôme

**Project:** Languagish: the Reverse Translating Double Dictionary Thesaurus

**Summary:** Don't know the word you are looking for? Languagish is a a browser extension that offers the best word given a verbal description. Even across different languages. (Languagish = Translator + Dictionary + Thesaurus


**Pitch Screenshot:** ![image](images/languagish.png)

**Code:** see [gpt3-reverse-thesaurus_0.0.3.zip on github](https://github.com/abett/gpt3-reverse-thesaurus/blob/main/gpt3-reverse-thesaurus_0.0.3.zip)

**Slides:** [here](./slides/languagish.pdf)



## Team: HealthyTunes

**Members:** Jens Bleiholder, Martin MH, Carsten Feuerbaum

**Project:** Weekly Fitness Plan Generator

**Summary:** A tool that generates workout recommendations for the next week given a few information such as name, age, weight, goal, etc. The tool also motivates you and gives optional music recommendations for your workout.

**Pitch Screenshot:**

**Code:** coming ...

**Slides:** coming ...


## **Team:** SLM-3

**Members:** Alfred, Irina Vidal, Jacob, Konrad, Ulli Waltinger

**Project:** Sign Language Machine on GPT3

**Summary:** An application that improves inclusivness by enabling to translate English phrases into American Sign Language instructions and contextualizing it with OpenAI`s very own CLIP images

**Pitch Screenshot:** ![image](images/slm3.png)

**Code:** coming ...

**Slides:** coming ...



## Team: AIComedians

**Members:** Philipp Gawlik, Sophia Antonin, Kathy Hämmerl, Anne Beyer, Anna Steinberg

**Project:** Joke Generator

**Summary:** Let GPT-3 generate answers to Lightbulb jokes and let it vote on its own and original answers.

**Pitch Screenshot:** ![image](images/AIComedians.png)

**Code:** coming ...

**Slides:** coming ...


## Team: SASS

**Members:** Swaneet Sahoo, Sarah Sultan, Adwait Dathan R

**Project:** Edubot

**Summary:** Interactive educational chatbot for exploring new topics. Information is enriched from recent data from the internet.

**Pitch Screenshot:** ![image](images/edubot.png)

**Code:**  [github.com/GollyTicker/Edubot-GPT-3](https://github.com/GollyTicker/Edubot-GPT-3)

**Slides:** coming ...


## Team: YYQHXQVDLXY

**Members:** Cosmin Novac, Alexandru Tudorica

**Project:** GPT3 as a creative writing toolset

**Summary:** Integrating GPT3 into a writer's toolkit called Storyline Creator and its advantages over the existing GPT2 features

**Pitch Screenshot:** ![image](images/YYQHXQVDLXY.png)

**Code:** coming ...

**Slides:** [this pptx](./slides/YYQHXQVDLXY_AI_Generated_Novel_SCENE_Continuation.pptx)
