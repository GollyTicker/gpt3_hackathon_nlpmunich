import openai
import os
openai.api_key = "API KEY"
response = openai.Completion.create(engine="davinci", prompt="This is a test", max_tokens=5)
#openai.api_key = os.environ["OPENAI_API_KEY"]


def four_line_lyrics(theme, artist=False, song_part="verse"):
    if artist == False: 
        response = openai.Completion.create(
        engine="davinci-instruct-beta",
        prompt="Write a song {} about \"{}\".\n\n{} 1:\n".format(str(song_part), str(theme), str(song_part)),
        temperature=0.7,
        max_tokens=64,
        top_p=1,
        frequency_penalty=0.8,
        presence_penalty=0.7
        )
        text = response['choices'][0]['text']
    else: 
        response = openai.Completion.create(
        engine="davinci-instruct-beta",
        prompt="Write a song {} about \"{}\" in the style of \"{}\".\n\n{} 1:\n".format(str(song_part), str(theme), str(artist), str(song_part)),
        temperature=0.7,
        max_tokens=64,
        top_p=1,
        frequency_penalty=0.8,
        presence_penalty=0.7
        )
        text = response['choices'][0]['text']
  
    lyrics = list(filter((" ").__ne__, text.split("\n")))
    lyrics = list(filter(("").__ne__, lyrics))
    #print(len(lyrics))

    if len(lyrics) < 4: 
        lyrics = four_line_lyrics(theme, artist)

    return lyrics  

def iterativley_add_lyric_line(lyrics, theme):
    str_1 = '"Write “Next Line:” to a song referencing previous “Lyrics:” and fulfil the following constraints:\n- The theme of the song line should be “{}”\n- The song line should rhyme with the previous song line.\n- Every \"Next line:\" should start with a different word.\n\n'.format(theme)
    
    str_2 = "Lyrics:\n"
    for line in lyrics[0:-2]:
        str_2 += line + "\n"
   
    str_3 = "\nNext Line:\n" + lyrics[-2] + "\n\n"

    str_4 = "Lyrics:\n"
    for line in lyrics[0:-1]:
        str_4 += line + "\n"
    str_5 = "\nNext Line:\n" + lyrics[-1] + "\n\n"


    str_6 = "Lyrics:\n"
    for line in lyrics[0:]:
        str_6 += line + "\n"
    str_7 = "\nNext Line:\n"

    request_string = str_1 + str_2 + str_3 + str_4 + str_5 + str_6 + str_7

    response = openai.Completion.create(
    engine="davinci-instruct-beta",
    prompt= request_string,
    temperature=0.4,
    max_tokens=10,
    top_p=1,
    frequency_penalty=1.0,
    presence_penalty=1.0
    )

    text = response['choices'][0]['text']
    
    if "\n" in text: 
            new_lyrics = list(filter((" ").__ne__, text.split("\n")))
            new_lyrics = list(filter(("").__ne__, new_lyrics))
    elif len(text) == 0: 
        new_lyrics = iterativley_add_lyric_line(lyrics, theme)
    else: 
        new_lyrics = [text]

    #print("REQUEST:",request_string)

    return lyrics + new_lyrics[0:1]

def get_lyrics(theme, song_length, artist=False, song_part="verse"): 
    ans = four_line_lyrics(theme, artist, song_part)
    current_length = len(ans)
    idx = 0 
    while song_length > current_length and idx < 200:
        idx += 1 #just to avoid infinite loop

        ans = iterativley_add_lyric_line(ans, theme=theme)

        if ans[-1] == ans[-2]: 
            ans = ans[0:-1]
            ans = ans + four_line_lyrics(theme, artist, song_part)
    
        try: 
            if ans[-1].split(' ')[0] == ans[-2].split(' ')[0]: 
                ans = ans + four_line_lyrics(theme, artist, song_part)

        except Exception as e: 
            print(e)

        
        #keep track of song length in lines 
        current_length = len(ans)
    return ans 
















# FLASK



from flask import Flask, request
from flask_cors import CORS, cross_origin
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/', methods=['GET', 'POST'])
@cross_origin()
def parse_request():
    artist = request.args.get('artist')
    theme = request.args.get('theme')
    length = int(request.args.get('length'))

    data = get_lyrics( theme, length, artist )
    print(data, "DATA")
    slist = ''
    for sent in data:
        slist += sent + '\n'
    return slist

